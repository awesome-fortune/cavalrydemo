#!/usr/bin/env bash

function ECHO() { printf "%b%s%b" '\n\e[1m' "$*" '\e[0m\n' ;}

source ~/.profile
nvm install v8.9.0
nvm use v8.9.0

ECHO "1. Fixing permissions for framework"
chown -R www-data:www-data \
        /var/www/storage \
/var/www/bootstrap/cache

ECHO "2. Running composer update...this might take a while (´・ω・｀)"
composer install --no-dev

ECHO "3. Running npm install and preparing assets"
npm install
npm run production
php artisan route:cache

ECHO "4. Inserting cron entry for 5 minute candles that runs every minute"
tee /var/spool/cron/crontabs/$(whoami) <<<"* * * * * php /var/www/artisan schedule:run >> /dev/null 2>&1"

ECHO "5. Preparing DB"
php artisan migrate:fresh --force

ECHO "6. Running scheduler"
php artisan schedule:run

php artisan optimize

ECHO "7. Running 5 second candle script"
setsid ./sync-5-sec-candles.sh & >/dev/null 2>&1 < /dev/null &

ECHO "Done"