<?php

namespace App\Http\Controllers\Oanda;

use App\Console\Commands\CavalryAppTool;
use App\Jobs\Oanda\Sync5minCandles;
use App\Jobs\Oanda\Sync5secCandles;
use App\Oanda\Helper\OandaApi;
use App\Oanda\Manager\OandaApiManager;
use App\Oanda\Models\Candle5minuteGranularity;
use App\Oanda\Models\Candle5secondGranularity;
use App\Oanda\Models\Instrument;
use App\Http\Controllers\Controller;
use App\Oanda\Transformers\Candle5MinuteGranularityTransformer;
use App\Oanda\Transformers\Candle5SecondGranularityTransformer;
use Illuminate\Support\Facades\Artisan;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class OandaApiController extends Controller
{
    /**
     * @var $oandaApiManager OandaApiManager
     */
    protected $oandaApiManager;

    /**
     * @var $fractal Manager
     */
    protected $fractal;

    /**
     * @var $candle5MinuteGranularityTransformer Candle5MinuteGranularityTransformer
     */
    protected $candle5MinuteGranularityTransformer;

    /**
     * @var $candle5SecondGranularityTransformer Candle5SecondGranularityTransformer
     */
    protected $candle5SecondGranularityTransformer;

    public function __construct(Manager $fractal, Candle5MinuteGranularityTransformer $candle5MinuteGranularityTransformer, Candle5SecondGranularityTransformer $candle5SecondGranularityTransformer) {
        $this->oandaApiManager = resolve(OandaApiManager::class);
        $this->fractal = $fractal;
        $this->candle5MinuteGranularityTransformer = $candle5MinuteGranularityTransformer;
        $this->candle5SecondGranularityTransformer = $candle5SecondGranularityTransformer;
    }

    public function showInstrumentList()
    {
        $instrumentList = Instrument::all();

        return response()->json($instrumentList);
    }

    public function showLatestCandles($granularity)
    {
        switch ($granularity) {
            case OandaApi::GRANULARITY_5_MINUTES:
                return $this->get5minuteGranularityCandles();
                break;
            case OandaApi::GRANULARITY_5_SECONDS:
                return $this->get5secondGranularityCandles();
                break;
            default:
                throw new \InvalidArgumentException('Provided value for granularity ('.$granularity.') is invalid');
                break;
        }
    }

    public function get5secondGranularityCandles($count = 100)
    {
        $candles = Candle5secondGranularity::orderBy('id', 'desc')->take($count)->get();
        $candles = $candles->reverse();
        $candles = new Collection($candles, $this->candle5SecondGranularityTransformer);
        $candles = $this->fractal->createData($candles);

        return $candles->toArray();
    }

    public function get5minuteGranularityCandles($count = 100)
    {
        $candles = Candle5minuteGranularity::orderBy('id', 'desc')->take($count)->get();
        $candles = $candles->reverse();
        $candles = new Collection($candles, $this->candle5MinuteGranularityTransformer);
        $candles = $this->fractal->createData($candles);

        return $candles->toArray();
    }
}
