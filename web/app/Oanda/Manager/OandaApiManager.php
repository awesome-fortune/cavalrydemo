<?php
/**
 * Created by PhpStorm.
 * User: fortune
 * Date: 2017/10/28
 * Time: 12:12
 */

namespace App\Oanda\Manager;

use App\Oanda\Helper\OandaApi;

class OandaApiManager
{
    /**
     * @var $oandaApi OandaApi
     */
    protected $oandaApi;

    public function __construct(OandaApi $oandaApi)
    {
        $this->oandaApi = $oandaApi;
    }

    /**
     * @param $granularity
     * @param string $instrument
     * @param string $price
     * @param int $count
     *
     * @return mixed|string
     */
    public function getCandles($granularity, $instrument = 'EUR_USD', $price = OandaApi::PRICE_BID, $count = 100)
    {
        $options = [
            'count' => $count,
            'granularity' => $granularity,
            'instrument' => $instrument,
            'price' => $price
        ];

        try {
            return $this->oandaApi->getCandles($options);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Get a list of tradable instruments from the Oanda Api
     * @return string|array
     */
    public function getInstrumentList()
    {
        try {
            return $this->oandaApi->getInstrumentList()['instruments'];
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}