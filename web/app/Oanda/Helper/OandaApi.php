<?php
/**
 * Created by PhpStorm.
 * User: fortune
 * Date: 2017/10/28
 * Time: 02:27
 */

namespace App\Oanda\Helper;

use GuzzleHttp\Client;

class OandaApi
{
    /**
     * @var $guzzleClient Client
     */
    protected $guzzleClient;

    protected $oandaApiKey;

    protected $oandaAccountId;

    const PRICE_MIDPOINT = 'M';
    const PRICE_BID = 'B';
    const PRICE_ASK = 'A';

    const GRANULARITY_5_SECONDS = 'S5';
    const GRANULARITY_1_MINUTE = 'M1';
    const GRANULARITY_3_MINUTES = 'M3';
    const GRANULARITY_5_MINUTES = 'M5';

    public function __construct()
    {
        $this->oandaApiKey    = config('services.oanda.api_key');
        $this->oandaAccountId = config('services.oanda.account_id');

        $config = [
            'base_uri'           => config('services.oanda.api_uri'),
            'http_errors'        => true,
            'verify'             => false,
            'connection_timeout' => config('services.oanda.connection_timeout'),
            'timeout'            => config('services.oanda.timeout'),
        ];

        $this->guzzleClient = new Client($config);
    }

    /**
     * Fetches candlestick data for an instrument
     *
     * @param array $options
     *
     * @return mixed
     */
    public function getCandles(array $options)
    {
        $url = 'instruments/' . $options['instrument'] . '/candles?price=' . $options['price'] . '&granularity=' . $options['granularity'] . '&count=' . $options['count'];
        return $this->doGetRequest($url);
    }

    /**
     * Get a list of tradable instruments
     * @return mixed
     */
    public function getInstrumentList()
    {
        $url = 'accounts/'.$this->oandaAccountId.'/instruments';
        return $this->doGetRequest($url);
    }

    /**
     * Helper method for GET requests to the Oanda API
     *
     * @param $endpoint
     *
     * @return mixed
     */
    private function doGetRequest($endpoint)
    {
        if (is_null($endpoint)) {
            throw new \InvalidArgumentException('Please provide an endpoint to call!');
        }

        $response = $this->guzzleClient->get($endpoint, [
            'headers' => [
                'Content-Type'  => 'application/json',
                'Cache-Control' => 'no-cache',
                'Authorization' => 'Bearer ' . $this->oandaApiKey,
                'Accept-Datetime-Format' => 'UNIX'
            ]
        ]);

        return json_decode($response->getBody(), true);
    }
}