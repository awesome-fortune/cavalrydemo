<?php
/**
 * Created by PhpStorm.
 * User: fortune
 * Date: 2017/10/30
 * Time: 01:23
 */

namespace App\Oanda\Transformers;


use App\Oanda\Models\Candle5secondGranularity;
use League\Fractal\TransformerAbstract;

class Candle5SecondGranularityTransformer extends TransformerAbstract
{
    public function transform(Candle5secondGranularity $candle5SecondGranularity)
    {
        return [
            'id' => (int)$candle5SecondGranularity->id,
            'instrument' => $candle5SecondGranularity->instrument,
            'time' => $candle5SecondGranularity->time,
            'bids' => [
                'open' => $candle5SecondGranularity->open,
                'close' => $candle5SecondGranularity->close,
                'low' => $candle5SecondGranularity->low,
                'high' => $candle5SecondGranularity->high
            ]
        ];
    }
}