<?php
/**
 * Created by PhpStorm.
 * User: fortune
 * Date: 2017/10/30
 * Time: 00:53
 */

namespace App\Oanda\Transformers;


use App\Oanda\Models\Candle5minuteGranularity;
use League\Fractal\TransformerAbstract;

class Candle5MinuteGranularityTransformer extends TransformerAbstract
{
    public function transform(Candle5minuteGranularity $candle5MinuteGranularity)
    {
        return [
            'id' => (int)$candle5MinuteGranularity->id,
            'instrument' => $candle5MinuteGranularity->instrument,
            'time' => $candle5MinuteGranularity->time,
            'bids' => [
                'open' => $candle5MinuteGranularity->open,
                'close' => $candle5MinuteGranularity->close,
                'low' => $candle5MinuteGranularity->low,
                'high' => $candle5MinuteGranularity->high
            ]
        ];
    }
}