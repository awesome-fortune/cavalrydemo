<?php

namespace App\Oanda\Models;

use Illuminate\Database\Eloquent\Model;

class Candle5minuteGranularity extends Model
{
    protected $table = 'candles_5minute_granularity';

    protected $fillable = [
        'instrument',
        'time',
        'high',
        'low',
        'close',
        'open'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
