<?php

namespace App\Oanda\Models;

use Illuminate\Database\Eloquent\Model;

class Instrument extends Model
{
    protected $fillable = [
        'name',
        'type',
        'display_name',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
