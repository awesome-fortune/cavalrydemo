<?php

namespace App\Oanda\Models;

use Illuminate\Database\Eloquent\Model;

class Candle5secondGranularity extends Model
{
    protected $table = 'candles_5second_granularity';

    protected $fillable = [
        'instrument',
        'time',
        'high',
        'low',
        'close',
        'open'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}