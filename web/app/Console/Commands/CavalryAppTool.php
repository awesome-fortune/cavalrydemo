<?php

namespace App\Console\Commands;

use App\Oanda\Helper\OandaApi;
use App\Oanda\Manager\OandaApiManager;
use App\Oanda\Models\Candle5minuteGranularity;
use App\Oanda\Models\Candle5secondGranularity;
use App\Oanda\Models\Instrument;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CavalryAppTool extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cavalry:app-tool {action}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used for interaction between the Oanda Api and the Cavalry demo app';

    /**
     * @var $oandaApiManager OandaApiManager
     */
    protected $oandaApiManager;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->oandaApiManager = resolve(OandaApiManager::class);
    }

    const ACTION_SYNC_INSTRUMENTS = 'sync-instruments-to-db';
    const ACTION_SYNC_5M_CANDLES = 'sync-5minute-candles-to-db';
    const ACTION_SYNC_5S_CANDLES = 'sync-5second-candles-to-db';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = $this->input->getArgument('action');
        switch ($action) {
            case self::ACTION_SYNC_INSTRUMENTS:
                $this->processSyncInstrumentsToDb();
                break;
            case self::ACTION_SYNC_5M_CANDLES:
                $this->processSync5minuteCandlesToDb();
                break;
            case self::ACTION_SYNC_5S_CANDLES:
                $this->processSync5SecondCandlesToDb();
                break;
            default:
                throw new \InvalidArgumentException('action ['.$action.'] not valid!');
                break;
        }
    }

    private function parseTimestamp($timestamp)
    {
        return date('Y-m-d H:i:s', $timestamp);
    }

    private function processSync5SecondCandlesToDb()
    {
        try {
            $candles = $this->oandaApiManager->getCandles(OandaApi::GRANULARITY_5_SECONDS);

            DB::beginTransaction();
            foreach ($candles['candles'] as $candle) {

                Candle5secondGranularity::updateOrCreate(
                    [
                        'instrument' => $candles['instrument'],
                        'time'       => $this->parseTimestamp($candle['time']),
                        'open'       => $candle['bid']['o'],
                        'close'      => $candle['bid']['c'],
                        'high'       => $candle['bid']['h'],
                        'low'        => $candle['bid']['l'],
                    ]
                );
            }

            DB::commit();
        } catch (\Exception $e) {
            $this->error('Rolling back - '.$e->getMessage());
            DB::rollback();
        }
    }

    private function processSync5minuteCandlesToDb()
    {
        try {
            $this->info('Getting candles from Oanda API');
            $candles = $this->oandaApiManager->getCandles(OandaApi::GRANULARITY_5_MINUTES);

            DB::beginTransaction();
            $this->info('Beginning DB transacion');

            foreach ($candles['candles'] as $candle) {

                Candle5minuteGranularity::updateOrCreate(
                    [
                        'instrument' => $candles['instrument'],
                        'time'       => $this->parseTimestamp($candle['time']),
                        'open'       => $candle['bid']['o'],
                        'close'      => $candle['bid']['c'],
                        'high'       => $candle['bid']['h'],
                        'low'        => $candle['bid']['l'],
                    ]
                );
            }

            DB::commit();
            $this->info('Commiting - Done');
        } catch (\Exception $e) {
            $this->error('Rolling back - '.$e->getMessage());
            DB::rollback();
        }

    }

    /**
     * Gets a list of instruments from the OandaApi and puts them in the DB
     */
    private function processSyncInstrumentsToDb()
    {
        try {
            $this->info('Getting instrument list');
            $instruments = $this->oandaApiManager->getInstrumentList();

            DB::beginTransaction();
            $this->info('Beginning DB transacion');

            foreach ($instruments as $instrument) {
                $this->info('Processing ['.$instrument['name'].'] item');

                Instrument::updateOrCreate(
                    [
                        'name'         => $instrument['name'],
                        'type'         => $instrument['type'],
                        'display_name' => $instrument['displayName'],
                    ]
                );
            }

            DB::commit();
            $this->info('Commiting - Done');
        } catch (\Exception $e) {
            $this->error('Rolling back - '.$e->getMessage());
            DB::rollback();
        }
    }
}
