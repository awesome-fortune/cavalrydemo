<?php

namespace App\Providers;

use App\Oanda\Manager\OandaApiManager;
use Illuminate\Support\ServiceProvider;
use App\Oanda\Helper\OandaApi;


class AppServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(OandaApiManager::class, function() {
            $oandaApi = new OandaApi();
            return new OandaApiManager($oandaApi);
        });
    }
}
