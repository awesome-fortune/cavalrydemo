#!/usr/bin/env bash
# This is a hack to run the 5 second sync command...can't do anything less than 60 seconds in cron - someday the hack might be paid back
while true
do
 php artisan cavalry:app-tool sync-5second-candles-to-db
 sleep 5
done