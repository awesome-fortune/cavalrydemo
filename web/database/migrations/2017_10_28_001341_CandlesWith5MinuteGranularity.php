<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CandlesWith5MinuteGranularity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candles_5minute_granularity', function(Blueprint $table) {
            $table->increments('id');
            $table->string('instrument');
            $table->double('open');
            $table->double('high');
            $table->double('low');
            $table->double('close');
            $table->dateTime('time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candles_5minute_granularity');
    }
}
