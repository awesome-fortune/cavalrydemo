(function () {
    $('#5-min-interval').on('click', function (e) {
        e.preventDefault();

        $('#5-sec-interval').removeClass('active');
        $(this).addClass('active');

        load5MinuteChart();
    });

    function load5MinuteChart() {
        $.getJSON('/api/latest-candles/M5', function (data) {
            var chartData = [];
            for (var i = 0; i < _.size(data.data); i++) {
                var date = moment(data.data[i].time).unix() * 1000;
                chartData.push([date, data.data[i].bids.close]);
            }

            var rangeSelectorButtons = [{
                count: 1,
                type: 'minute',
                text: '1M'
            }, {
                count: 5,
                type: 'minute',
                text: '5M'
            }, {
                type: 'all',
                text: 'All'
            }];

            Highcharts.stockChart('5-minute-chart', {
                chart: {
                    height: 600,
                    events: {
                        load: function () {
                            var series = this.series[0];
                            setInterval(function () {
                                $.ajax({
                                    url: '/api/latest-candles/M5',
                                    success: function (response) {
                                        var time = moment(response.data[_.size(response.data) - 1].time).unix() * 1000;
                                        var closingPrice = response.data[_.size(response.data) - 1].bids.close;
                                        series.addPoint([time, closingPrice], true, true);
                                    }
                                });
                            }, 30000);
                        }
                    }
                },

                rangeSelector: {
                    buttons: rangeSelectorButtons,
                    inputEnabled: false,
                    selected: 2
                },

                title: {
                    text: 'EUR_USD Stock Price'
                },

                subtitle: {
                    text: '5 Minutes change interval'
                },

                exporting: {
                    enabled: false
                },

                series: [{
                    name: 'EUR_USD',
                    data: chartData
                }]
            });

            $('#5-second-chart').hide();
            $('#5-minute-chart').show();
        });
    }
})();