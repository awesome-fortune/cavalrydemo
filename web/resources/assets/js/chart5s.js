(function () {
    load5SecondChart();

    $('#5-sec-interval').on('click', function (e) {
        e.preventDefault();

        $('#5-min-interval').removeClass('active');
        $(this).addClass('active');

        load5SecondChart();
    });

    function load5SecondChart() {
        $.getJSON('/api/latest-candles/S5', function (data) {
            var sumClosingPrice = 0;
            var chartData = [];
            for (var i = 0; i < _.size(data.data); i++) {
                var date = moment(data.data[i].time).unix() * 1000;
                chartData.push([date, data.data[i].bids.close]);
                sumClosingPrice += data.data[i].bids.close;
            }

            var avgClosingPrice = sumClosingPrice / 100;

            $('#open-price').html(data.data[_.size(data.data) - 1].bids.open);
            $('#close-price').html(data.data[_.size(data.data) - 1].bids.close);
            $('#high-price').html(data.data[_.size(data.data) - 1].bids.high);
            $('#low-price').html(data.data[_.size(data.data) - 1].bids.low);
            $('#avg-close-price').html(avgClosingPrice.toFixed(5));

            var rangeSelectorButtons = [{
                count: 1,
                type: 'minute',
                text: '1M'
            }, {
                count: 5,
                type: 'minute',
                text: '5M'
            }, {
                type: 'all',
                text: 'All'
            }];

            Highcharts.stockChart('5-second-chart', {
                chart: {
                    height: 600,
                    events: {
                        load: function () {
                            var series = this.series[0];
                            setInterval(function () {
                                $.ajax({
                                    url: '/api/latest-candles/S5',
                                    success: function (response) {
                                        var time = moment(response.data[_.size(response.data) - 1].time).unix() * 1000;
                                        var closingPrice = response.data[_.size(response.data) - 1].bids.close;
                                        series.addPoint([time, closingPrice], true, true);

                                        var sumClosingPrice = 0;
                                        for (var i = 0; i < _.size(response.data); i++) {
                                            sumClosingPrice += response.data[i].bids.close;
                                        }

                                        var avgClosingPrice = sumClosingPrice / 100;

                                        $('#open-price').html(response.data[response.data.length -1].bids.open);
                                        $('#close-price').html(response.data[response.data.length -1].bids.close);
                                        $('#high-price').html(response.data[response.data.length -1].bids.high);
                                        $('#low-price').html(response.data[response.data.length -1].bids.low);
                                        $('#avg-close-price').html(avgClosingPrice.toFixed(5));
                                    }
                                });
                            }, 5000);
                        }
                    }
                },

                rangeSelector: {
                    buttons: rangeSelectorButtons,
                    inputEnabled: false,
                    selected: 2
                },

                title: {
                    text: 'EUR_USD Stock Price'
                },
                subtitle: {
                    text: '5 Seconds change interval'
                },

                exporting: {
                    enabled: false
                },

                series: [{
                    name: 'EUR_USD',
                    data: chartData
                }]
            });

            $('#5-second-chart').show();
            $('#5-minute-chart').hide();
        });
    }
})();