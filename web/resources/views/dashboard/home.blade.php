@extends('layout.base')

@section('title', 'Dashboard')

@section('content')
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">

                <li id="5-sec-interval">
                    <a href="#">5 Second interval</a>
                </li>
                <li id="5-min-interval">
                    <a href="#">5 Minute interval</a>
                </li>
            </ul>

        </div>
        <div class="col-sm-9 col-sm-offset-4 col-md-10 col-md-offset-2 main">
            <div class="row tile_count">
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span><i class="fa fa-hourglass-start"></i> Open</span>
                    <div class="count" id="open-price"></div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-hourglass-end"></i> Close</span>
                    <div class="count" id="close-price"></div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-angle-double-up"></i> High</span>
                    <div class="count green" id="high-price"></div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-angle-double-down"></i> Low</span>
                    <div class="count" id="low-price"></div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                    <span class="count_top"><i class="fa fa-bar-chart"></i> Average closing price</span>
                    <div class="count" id="avg-close-price"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Cavalry Dashboard</h1>
            <div id="5-minute-chart" class="row"></div>
            <div id="5-second-chart" class="row"></div>
        </div>
    </div>
@stop

@section('javascripts')
    @parent
    <script>
        $(window).on('load', function () { // makes sure the whole site is loaded
            $('#status').fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
            $('body').delay(350).css({'overflow': 'visible'});

            $('#5-sec-interval').addClass('active');


            if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
                var msViewportStyle = document.createElement('style');
                msViewportStyle.appendChild(
                    document.createTextNode(
                        '@-ms-viewport{width:auto!important}'
                    )
                );

                $('head').appendChild(msViewportStyle)
            }
        });
    </script>
@stop