<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cavalry - @yield('title')</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script src="https://use.fontawesome.com/0c35edde57.js"></script>
</head>
<body>
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
    <div class="container-fluid">
        @yield('content')
    </div>

    @section('javascripts')
        <script src="https://code.highcharts.com/stock/highstock.js"></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
        <script src="{{ mix('js/app.js') }}"></script>
    @show
</body>
</html>