# Cavalry Demo App
<p align="center"><img src="http://cavalryapps.com/CavalryMediaBrand.svg"></p>
A simple forex rate recorder wit a live price monitor frontend.

### Prerequisites
- [Git](https://git-scm.com/)
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Getting Started

Please follow these instructions in order to get the project up and running on your machine.

1. Clone the repository by running
`git clone https://awesome-fortune@bitbucket.org/awesome-fortune/cavalrydemo.git`
2. Navigate into `cavalrydemo/dockerfiles` and run `docker-compose up -d`
3. ssh into the php_fpm container by running `docker exec -it cavalry_php_fpm /bin/bash`
4. Let the app have its glory by running `bash boot.sh` 
5. Access the web app on [`http://localhost:8999`](http://localhost:8999)

## TODO

- Tests
- Mobile app
- Container orchestration 
- Pay back the 5 second sync script hack... (´・ω・｀)